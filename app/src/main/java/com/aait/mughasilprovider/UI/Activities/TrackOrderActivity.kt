package com.aait.mughasilprovider.UI.Activities

import android.os.Build
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.mughasilprovider.Base.Parent_Activity
import com.aait.mughasilprovider.Client
import com.aait.mughasilprovider.Models.AboutAppResponse
import com.aait.mughasilprovider.Network.Service
import com.aait.mughasilprovider.R
import com.aait.mughasilprovider.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TrackOrderActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_track_order
    lateinit var menu: ImageView
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var one: ImageView
    lateinit var two: ImageView
    lateinit var three: ImageView
    lateinit var work_underway:Button
    lateinit var finish:Button
    var id = 0

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        menu = findViewById(R.id.menu)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        three = findViewById(R.id.three)
        work_underway = findViewById(R.id.work_under_way)
        finish = findViewById(R.id.finish)
        menu.visibility = View.GONE
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = "متابعة الطلب"
        Track(null, null)
        work_underway.setOnClickListener { Track(1,"work_underway") }
        finish.setOnClickListener { Track(1,"completed") }

    }

    fun Track(confirm:Int?,status:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.track(mSharedPrefManager.userData.id!!,id,confirm,status)?.enqueue(object :
            Callback<AboutAppResponse> {
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data.equals("pending")){
                            one.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            two.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            three.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            one.setImageResource(R.mipmap.path222)
                            work_underway.visibility = View.VISIBLE
                            finish.visibility = View.GONE
                        }else if (response.body()?.data.equals("work_underway")){
                            one.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            two.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            three.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            one.setImageResource(R.mipmap.path222)
                            two.setImageResource(R.mipmap.path222)
                            work_underway.visibility = View.GONE
                            finish.visibility = View.VISIBLE
                        }else if (response.body()?.data.equals("completed")){
                            one.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            two.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            three.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            one.setImageResource(R.mipmap.path222)
                            two.setImageResource(R.mipmap.path222)
                            three.setImageResource(R.mipmap.path222)
                            work_underway.visibility = View.GONE
                            finish.visibility = View.GONE
                        }else{
                            one.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            two.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            three.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            work_underway.visibility = View.GONE
                            finish.visibility = View.GONE
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}