package com.aait.mughasilprovider.UI.Activities

import android.Manifest
import android.app.Service
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.aait.mughasilprovider.Client

import com.aait.mughasilprovider.GPS.GPSTracker
import com.aait.mughasilprovider.GPS.GpsTrakerListener
import com.aait.mughasilprovider.Models.UserResponse
import com.aait.mughasilprovider.Perefereces.LanguagePrefManager
import com.aait.mughasilprovider.Perefereces.SharedPrefManager
import com.aait.mughasilprovider.R
import com.aait.mughasilprovider.Uitls.CommonUtil
import com.aait.mughasilprovider.Uitls.DialogUtil
import com.aait.mughasilprovider.Uitls.PermissionUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.io.IOException
import java.util.*

class ProfileService: Service() {
    lateinit var mSharedPrefManager: SharedPrefManager

    lateinit var mLanguagePrefManager: LanguagePrefManager
    lateinit var mContext: Context
    var result = ""
    var mLat = ""
    var mLang = ""
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        mContext = this
        mSharedPrefManager = SharedPrefManager(mContext)
        mLanguagePrefManager = LanguagePrefManager(mContext)
        if (intent.hasExtra("result")) {
            var b = Bundle()
            b = intent.extras!!
             result = b.getString("result")!!
        }
        if (intent.hasExtra("mLat")) {
            var b1 = Bundle()
            b1 = intent.extras!!
            mLat = b1.getString("mLat")!!
        }
        if (intent.hasExtra("mLang")) {
            var b2 = Bundle()
            b2 = intent.extras!!
            mLang = b2.getString("mLang")!!
        }

        //onTaskRemoved(intent)
        Client.getClient()?.create(com.aait.mughasilprovider.Network.Service::class.java)?.updateLat(mSharedPrefManager.userData.id!!,mLanguagePrefManager.appLanguage
            ,result!!,mLat!!,mLang!!)?.enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()

            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {

                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){

                        mSharedPrefManager.userData = response.body()?.data!!

                    }else{

                    }
                }
            }

        })

//        Toast.makeText(
//            applicationContext, "This is a Service running in Background",
//            Toast.LENGTH_SHORT
//        ).show()
        return START_STICKY
    }
    override fun onBind(intent: Intent): IBinder? {

        throw UnsupportedOperationException("Not yet implemented")
    }
    override fun onTaskRemoved(rootIntent: Intent) {
        val restartServiceIntent = Intent(applicationContext, this.javaClass)
        restartServiceIntent.setPackage(packageName)
        startService(restartServiceIntent)
        super.onTaskRemoved(rootIntent)
    }

    override fun onCreate() {
        super.onCreate()
        mContext = this
        mSharedPrefManager = SharedPrefManager(mContext)
        mLanguagePrefManager = LanguagePrefManager(mContext)


//        result =
//         mLat =b.getString("mLat")
//         mLang =b.getString("mLang")
    }
}