package com.aait.mughasilprovider.UI.Activities

import android.Manifest
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.aait.mughasilprovider.Base.Parent_Activity
import com.aait.mughasilprovider.Client
import com.aait.mughasilprovider.GPS.GPSTracker
import com.aait.mughasilprovider.GPS.GpsTrakerListener
import com.aait.mughasilprovider.Models.AboutAppResponse
import com.aait.mughasilprovider.Models.ConversationIdResponse
import com.aait.mughasilprovider.Models.OrderDetailsModel
import com.aait.mughasilprovider.Models.OrderDetailsResponse
import com.aait.mughasilprovider.Network.Service
import com.aait.mughasilprovider.R
import com.aait.mughasilprovider.Uitls.CommonUtil
import com.aait.mughasilprovider.Uitls.DialogUtil
import com.aait.mughasilprovider.Uitls.PermissionUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class OrderDetailsActivity:Parent_Activity(),GpsTrakerListener {
    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // Log.e("lat",lat.toString()+" "+log.toString())
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    override val layoutResource: Int
        get() = R.layout.activity_order_details
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    private var mAlertDialog: AlertDialog? = null
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var name:TextView
    lateinit var address:TextView
    lateinit var accept:Button
    lateinit var refuse:Button
    lateinit var chat:Button
    var id = 0
    lateinit var orderDetailsModel:OrderDetailsModel
    var lat = ""
    var lng = ""

    override fun initializeComponents() {
       id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        name = findViewById(R.id.name)
        address = findViewById(R.id.address)
        accept = findViewById(R.id.accept)
        refuse = findViewById(R.id.refuse)
        chat = findViewById(R.id.chat)
        title.text = "تفاصيل الطلب"
        back.setOnClickListener { onBackPressed()
        finish()}
        getLocationWithPermission()
        getOrder()
        accept.setOnClickListener { Accept() }
        refuse.setOnClickListener { Refuse() }
        chat.setOnClickListener { getID() }
        address.setOnClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?saddr=" + mLat + "," + mLang + "&daddr=" + lat + "," + lng)
                )
            )
//            val gmmIntentUri = Uri.parse("geo:"+lat+","+lng)
//            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
//            mapIntent.setPackage("com.google.android.apps.maps")
//            startActivity(mapIntent)
        }

    }
    fun getOrder(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.OrderDetails(mSharedPrefManager.userData.id!!,id,null)?.enqueue(object :
            Callback<OrderDetailsResponse>{
            override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<OrderDetailsResponse>,
                response: Response<OrderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        orderDetailsModel = response.body()?.data!!
                        name.text = response.body()?.data?.username
                        address.text = response.body()?.data?.address
                        lat = response.body()?.data?.lat!!
                        lng = response.body()?.data?.lng!!
                        if (response.body()?.data?.status!!.equals("current")){
                            accept.visibility = View.VISIBLE
                            refuse.visibility = View.VISIBLE
                            chat.visibility = View.GONE
                        }else if (response.body()?.data?.status!!.equals("pending")){
                            accept.visibility = View.VISIBLE
                            refuse.visibility = View.VISIBLE
                            chat.visibility = View.VISIBLE
                        }else{
                            accept.visibility = View.GONE
                            refuse.visibility = View.GONE
                            chat.visibility = View.GONE
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
    fun Accept(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.OrderDetails(mSharedPrefManager.userData.id!!,id,"accepted")?.enqueue(object :
            Callback<OrderDetailsResponse>{
            override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<OrderDetailsResponse>,
                response: Response<OrderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        startActivity(Intent(this@OrderDetailsActivity,MainActivity::class.java))
                        this@OrderDetailsActivity.finish()
                    }else if (response.body()?.value.equals("2")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        show()
                    }
                }
            }
        })
    }
    fun Refuse(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.OrderDetails(mSharedPrefManager.userData.id!!,id,"refuse")?.enqueue(object :
            Callback<OrderDetailsResponse>{
            override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<OrderDetailsResponse>,
                response: Response<OrderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        startActivity(Intent(this@OrderDetailsActivity,MainActivity::class.java))
                        this@OrderDetailsActivity.finish()
                    }else if (response.body()?.value.equals("2")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        show()
                    }
                }
            }
        })
    }
    fun getID(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ConversationID(mSharedPrefManager.userData.id!!,orderDetailsModel.user_id!!)?.enqueue(object :
            Callback<ConversationIdResponse>{
            override fun onFailure(call: Call<ConversationIdResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ConversationIdResponse>,
                response: Response<ConversationIdResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@OrderDetailsActivity,ChatActivity::class.java)
                        intent.putExtra("id",response.body()?.data?.conversation_id)
                        intent.putExtra("receiver",response.body()?.data?.receiver_id)
                        intent.putExtra("lastpage",response.body()?.data?.lastPage)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
    fun show(){
        val dialog = Dialog(mContext)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog ?.setCancelable(false)
        dialog ?.setContentView(R.layout.dialog_delete)
        val yes = dialog?.findViewById<Button>(R.id.agree)
        val no = dialog?.findViewById<Button>(R.id.refuse)
        no?.setOnClickListener { dialog?.dismiss() }
        yes?.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.deleteOrder(mSharedPrefManager.userData.id!!,id,"request")?.enqueue(object :Callback<AboutAppResponse>{
                override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<AboutAppResponse>,
                    response: Response<AboutAppResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            dialog?.dismiss()
                            startActivity(Intent(this@OrderDetailsActivity,MainActivity::class.java))
                            finish()
                        }else{
                            dialog?.dismiss()
                        }
                    }
                }

            })
        }
        dialog?.show()
    }

    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)





                    }


                } catch (e: IOException) {
                }



            }
        }
    }
}