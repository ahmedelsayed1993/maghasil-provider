package com.aait.mughasilprovider.UI.Activities

import android.content.Intent
import android.util.Log
import com.aait.mughasilprovider.Base.Parent_Activity
import com.aait.mughasilprovider.Client
import com.aait.mughasilprovider.Models.OrderDetailsResponse
import com.aait.mughasilprovider.Network.Service
import com.aait.mughasilprovider.R
import com.aait.mughasilprovider.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RefuseActivity : Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_order

    var id = ""

    override fun initializeComponents() {
        id = intent.getStringExtra("id")
        Log.e("id",id.toString())

        Accept()

    }
    fun Accept(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.OrderDetails(mSharedPrefManager.userData.id!!,id.toInt(),"refuse")?.enqueue(object :
            Callback<OrderDetailsResponse> {
            override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<OrderDetailsResponse>,
                response: Response<OrderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        startActivity(Intent(this@RefuseActivity,MainActivity::class.java))
                        this@RefuseActivity.finish()
                    }else {
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        startActivity(Intent(this@RefuseActivity,MainActivity::class.java))
                        this@RefuseActivity.finish()
                    }
                }
            }
        })
    }
}