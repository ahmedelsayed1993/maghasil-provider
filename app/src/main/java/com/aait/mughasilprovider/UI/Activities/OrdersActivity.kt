package com.aait.mughasilprovider.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.mughasilprovider.Base.Parent_Activity
import com.aait.mughasilprovider.Client
import com.aait.mughasilprovider.Listeners.OnItemClickListener
import com.aait.mughasilprovider.Models.OrderModel
import com.aait.mughasilprovider.Models.OrdersResponse
import com.aait.mughasilprovider.Network.Service
import com.aait.mughasilprovider.R
import com.aait.mughasilprovider.UI.Adapters.OrdersAdapter
import com.aait.mughasilprovider.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrdersActivity:Parent_Activity() ,OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        if (status.equals("pending")){
            val intent = Intent(this,FinishedOrderActivity::class.java)
            intent.putExtra("id",chatModels.get(position).id)
            startActivity(intent)
        }else if (status.equals("deleted_provider")) {
        }else{
            val intent = Intent(this, OrderDetailsActivity::class.java)
            intent.putExtra("id", chatModels.get(position).id)
            startActivity(intent)
        }

    }

    override val layoutResource: Int
        get() = R.layout.activity_orders
    lateinit var title: TextView
    lateinit var menu: ImageView
    lateinit var rv_recycle: RecyclerView
    lateinit var back:ImageView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var chatModels = java.util.ArrayList<OrderModel>()
    internal lateinit var ordersAdapter: OrdersAdapter
    var status = ""

    override fun initializeComponents() {
        status = intent.getStringExtra("status")
        title = findViewById(R.id.title)
        menu = findViewById(R.id.menu)
        title.text = getString(R.string.orders)
        back  = findViewById(R.id.back)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}
        menu.visibility = View.GONE
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        ordersAdapter =  OrdersAdapter(mContext,chatModels,R.layout.recycler_order)
        ordersAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = ordersAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome() }
        getHome()
    }

    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Orders(mSharedPrefManager.userData.id!!,status)?.enqueue(object :
            Callback<OrdersResponse> {
            override fun onResponse(call: Call<OrdersResponse>, response: Response<OrdersResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                ordersAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<OrdersResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })


    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
        finish()

    }
}