package com.aait.mughasilprovider.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aait.mughasilprovider.Base.ParentRecyclerAdapter
import com.aait.mughasilprovider.Base.ParentRecyclerViewHolder
import com.aait.mughasilprovider.Models.ChatsModel
import com.aait.mughasilprovider.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class ConversationsAdapter (context: Context, data: MutableList<ChatsModel>, layoutId: Int) :
    ParentRecyclerAdapter<ChatsModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val chatsModel = data.get(position)
        viewHolder.name!!.setText(chatsModel.username)
        viewHolder.message!!.text = chatsModel.message!!
        //  Glide.with(mcontext).load(listModel.image!!).into(viewHolder.photo)
        Glide.with(mcontext).load(chatsModel.avatar).into(viewHolder.image)
        if (chatsModel.seen==1){
            viewHolder.seen.setImageResource(R.mipmap.path222)
        }else{
            viewHolder.seen.setImageResource(R.mipmap.rr)
        }
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var image=itemView.findViewById<CircleImageView>(R.id.image)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var message = itemView.findViewById<TextView>(R.id.message)
        internal var seen = itemView.findViewById<ImageView>(R.id.seen)



    }
}