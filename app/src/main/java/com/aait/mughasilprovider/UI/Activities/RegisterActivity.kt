package com.aait.mughasilprovider.UI.Activities

import android.content.Intent
import android.os.Build
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.aait.mughasilprovider.Base.Parent_Activity
import com.aait.mughasilprovider.Client
import com.aait.mughasilprovider.Models.UserResponse
import com.aait.mughasilprovider.Network.Service
import com.aait.mughasilprovider.R
import com.aait.mughasilprovider.Uitls.CommonUtil
import com.aait.mughasilprovider.Uitls.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.firebase.iid.FirebaseInstanceId
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class RegisterActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_register
    lateinit var image:CircleImageView
    lateinit var phone:EditText
    lateinit var name:EditText
    lateinit var mail:EditText
    lateinit var location:TextView
    lateinit var another_phone:EditText
    lateinit var commercial:EditText
    lateinit var password:EditText
    lateinit var confirm:EditText
    lateinit var register:Button
    var lat = ""
    var lng = ""
    var address = ""
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath: String? = null
    var deviceID = ""
    var email = ""
    var userName = ""

    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        email = intent.getStringExtra("mail")
        userName = intent.getStringExtra("name")
        image = findViewById(R.id.image)
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.name)
        mail = findViewById(R.id.mail)
        location = findViewById(R.id.location)
        another_phone = findViewById(R.id.another_phone)
        commercial = findViewById(R.id.commercial)
        password = findViewById(R.id.password)
        confirm = findViewById(R.id.confirm)
        register = findViewById(R.id.register)
        mail.setText(email)
        name.setText(userName)

        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }

        location.setOnClickListener { startActivityForResult(Intent(this@RegisterActivity,LocationActivity::class.java),1) }

        register.setOnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                    CommonUtil.checkLength1(phone,getString(R.string.phone_length),10)||
                    CommonUtil.checkEditError(name,"اسم المغسلة")||
                    CommonUtil.checkTextError(location,"العنوان")||
                    CommonUtil.checkEditError(commercial,getString(R.string.commercial_no))||
                CommonUtil.checkLength1(commercial,"رقم السجل يجب ان يكون عشرة ارقام",10)||
                    CommonUtil.checkEditError(password,getString(R.string.password))||
                    CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                    CommonUtil.checkEditError(confirm,getString(R.string.confirm_password))){
                return@setOnClickListener
            }else{
                if (!password.text.toString().equals(confirm.text.toString())){
                    confirm.error = getString(R.string.password_not_match)
                }else{
                    if(!mail.text.toString().equals("")){
                        if (!another_phone.text.toString().equals("")){
                            if (!CommonUtil.isEmailValid(mail,getString(R.string.correct_email))||
                                    CommonUtil.checkLength1(another_phone,getString(R.string.phone_length),10)){
                                return@setOnClickListener
                            }else{
                                if (ImageBasePath!=null){
                                    Register(mail.text.toString(),another_phone.text.toString(),ImageBasePath!!)
                                }else{
                                    CommonUtil.makeToast(mContext,"اضف صورة من فضلك")
                                }
                            }
                        }else{
                            if (!CommonUtil.isEmailValid(mail,getString(R.string.correct_email))){
                                return@setOnClickListener
                            }else{
                                if (ImageBasePath!=null){
                                    Register(mail.text.toString(),null,ImageBasePath!!)
                                }else{
                                    CommonUtil.makeToast(mContext,"اضف صورة من فضلك")
                                }
                            }
                        }
                    }else{
                        if (!another_phone.text.toString().equals("")){
                            if (
                                CommonUtil.checkLength1(another_phone,getString(R.string.phone_length),10)){
                                return@setOnClickListener
                            }else{
                                if (ImageBasePath!=null){
                                    Register(null,another_phone.text.toString(),ImageBasePath!!)
                                }else{
                                    CommonUtil.makeToast(mContext,"اضف صورة من فضلك")
                                }
                            }
                        }else{

                                if (ImageBasePath!=null){
                                    Register(null,null,ImageBasePath!!)

                                }else{
                                    CommonUtil.makeToast(mContext,"اضف صورة من فضلك")
                                }

                        }
                    }

                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode==0){

            }else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]

                Glide.with(mContext).load(ImageBasePath).into(image)

                if (ImageBasePath != null) {
                    // upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }else{
            if (data?.getStringExtra("result")!=null) {
                address = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                location.text = address
            }else{
                address = ""
                lat = ""
                lng = ""
                location.text = address
            }
        }

    }
    fun Register(email:String?,another:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SignUp(phone.text.toString(),name.text.toString(),email,address,lat,lng,another,commercial.text.toString(),password.text.toString(),
            deviceID,"android")?.enqueue(object : Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent  = Intent(this@RegisterActivity,ActiviationActivity::class.java)
                        intent.putExtra("data",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun Register(email:String?,another:String?,path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.SignUpwithImage(phone.text.toString(),name.text.toString(),email,address,lat,lng,another,commercial.text.toString(),password.text.toString(),
            deviceID,"android",filePart)?.enqueue(object : Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent  = Intent(this@RegisterActivity,ActiviationActivity::class.java)
                        intent.putExtra("data",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}