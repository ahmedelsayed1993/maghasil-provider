package com.aait.mughasilprovider.UI.Activities

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import com.aait.mughasilprovider.Base.Parent_Activity
import com.aait.mughasilprovider.Client
import com.aait.mughasilprovider.Models.AboutAppResponse
import com.aait.mughasilprovider.Network.Service
import com.aait.mughasilprovider.R
import com.aait.mughasilprovider.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AboutAppActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_about_app
    lateinit var about:TextView
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var menu:ImageView
    override fun initializeComponents() {
        about = findViewById(R.id.about)
        title = findViewById(R.id.title)
        title.text = getString(R.string.about_app)
        menu = findViewById(R.id.menu)
        back = findViewById(R.id.back)
        menu.setOnClickListener { onBackPressed()
        finish()}
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}
        getAbout()
    }
    fun getAbout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AboutApp()?.enqueue(object :
            Callback<AboutAppResponse> {
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        about.text = response.body()?.data!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}