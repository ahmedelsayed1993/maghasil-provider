package com.aait.mughasilprovider.UI.Activities

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.Gravity
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.view.GravityCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.mughasilprovider.Base.Parent_Activity
import com.aait.mughasilprovider.Client
import com.aait.mughasilprovider.GPS.GPSTracker
import com.aait.mughasilprovider.GPS.GpsTrakerListener
import com.aait.mughasilprovider.Models.AboutAppResponse
import com.aait.mughasilprovider.Models.BaseResponse
import com.aait.mughasilprovider.Models.MainResponse
import com.aait.mughasilprovider.Models.UserResponse
import com.aait.mughasilprovider.Network.Service
import com.aait.mughasilprovider.R
import com.aait.mughasilprovider.Uitls.CommonUtil
import com.aait.mughasilprovider.Uitls.DialogUtil
import com.aait.mughasilprovider.Uitls.PermissionUtils
import com.bumptech.glide.Glide
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class MainActivity:Parent_Activity() , GpsTrakerListener {
    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // Log.e("lat",lat.toString()+" "+log.toString())
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    private var mAlertDialog: AlertDialog? = null
    override val layoutResource: Int
        get() = R.layout.activity_main
    lateinit var about_app: LinearLayout
    lateinit var terms: LinearLayout
    lateinit var call_us: LinearLayout
    lateinit var settings: LinearLayout
    lateinit var chats: LinearLayout
    lateinit var orders: LinearLayout
    lateinit var profile: LinearLayout
    lateinit var notification: LinearLayout
    lateinit var image: CircleImageView
    lateinit var name: TextView
    lateinit var address: TextView
    lateinit var menu: ImageView
    lateinit var notify: ImageView
    lateinit var photo: CircleImageView
    lateinit var user_name: TextView
    lateinit var date: TextView
    lateinit var home:LinearLayout
    lateinit var logout: Button
    lateinit var new_lay:LinearLayout
    lateinit var new_order:TextView
    lateinit var pending_lay:LinearLayout
    lateinit var current_order:TextView
    lateinit var canceled_lay:LinearLayout
    lateinit var canceled_order:TextView
    lateinit var finished_lay:LinearLayout
    lateinit var finished_order:TextView
    lateinit var deleted_lay:LinearLayout
    lateinit var deleted_order:TextView
    var swipeRefresh: SwipeRefreshLayout? = null

    var deviceID = ""
//    lateinit var mainHandler: Handler
//
//    private val updateTextTask = object : Runnable {
//        override fun run() {
//            getLocationWithPermission()
//            mainHandler.postDelayed(this, 1000)
//        }
//    }
    private lateinit var timer: Timer
    private val noDelay = 0L
    private val everyFiveSeconds = 60000L

    override fun onResume() {
        super.onResume()

        val timerTask = object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    Log.e("rrr","rrrrrr")
                    getLocationWithPermission() }
            }
        }

        timer = Timer()
        timer.schedule(timerTask, noDelay, everyFiveSeconds)
    }

    override fun onPause() {
        super.onPause()

//        timer.cancel()
//        timer.purge()
    }
    @RequiresApi(Build.VERSION_CODES.O)
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        Log.e("user",Gson().toJson(mSharedPrefManager.userData))
        menu = findViewById(R.id.menu)
        notify = findViewById(R.id.notify)
        photo = findViewById(R.id.photo)
        user_name = findViewById(R.id.user_name)
        date = findViewById(R.id.date)
        new_lay = findViewById(R.id.new_lay)
        new_order = findViewById(R.id.new_order)
        pending_lay = findViewById(R.id.pending_lay)
        current_order = findViewById(R.id.current_order)
        canceled_lay = findViewById(R.id.canceled_lay)
        canceled_order = findViewById(R.id.canceled_order)
        finished_lay = findViewById(R.id.finished_lay)
        finished_order = findViewById(R.id.finished_order)
        deleted_lay = findViewById(R.id.deleted_lay)
        deleted_order = findViewById(R.id.deleted_order)
        swipeRefresh = findViewById(R.id.swipe_refresh)

//        Handler().postDelayed({
//
//            Handler().postDelayed({
//
//                getLocationWithPermission()
//
//            }, 30000)
//        }, 30000)

        date.text = LocalDateTime.now().format(DateTimeFormatter.ofPattern("EEEE ,dd MMMM YYYY")).toString()
        notify.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java)) }
        sideMenu()
        Profile()
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { Count() }

        Count()
        new_lay.setOnClickListener {
            val intent = Intent(this,OrdersActivity::class.java)
            intent.putExtra("status","current")
            startActivity(intent)
            finish()
        }
        pending_lay.setOnClickListener {
            val intent = Intent(this,OrdersActivity::class.java)
            intent.putExtra("status","pending")
            startActivity(intent)
            finish()
        }
        canceled_lay.setOnClickListener {
            val intent = Intent(this,OrdersActivity::class.java)
            intent.putExtra("status","refuse")
            startActivity(intent)
            finish()
        }

        finished_lay.setOnClickListener {
            val intent = Intent(this,OrdersActivity::class.java)
            intent.putExtra("status","accepted")
            startActivity(intent)
            finish()
        }

        deleted_lay.setOnClickListener {
            val intent = Intent(this,OrdersActivity::class.java)
            intent.putExtra("status","deleted_provider")
            startActivity(intent)
            finish()
        }
    }

    fun sideMenu(){
        drawer_layout.useCustomBehavior(Gravity.START)
        drawer_layout.useCustomBehavior(Gravity.END)
        drawer_layout.setRadius(Gravity.START, 50f)
        drawer_layout.setRadius(Gravity.END, 50f)
        drawer_layout.setViewScale(Gravity.START, 0.95f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewScale(Gravity.END, 0.95f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewElevation(Gravity.START, 0f) //set main view elevation when drawer open (dimension)
        drawer_layout.setViewElevation(Gravity.END, 0f) //set main view elevation when drawer open (dimension)
//        drawer_layout.setViewScrimColor(Gravity.START, Color.) //set drawer overlay coloe (color)
//        drawer_layout.setViewScrimColor(Gravity.END, Color.TRANSPARENT) //set drawer overlay coloe (color)
        drawer_layout.setDrawerElevation(Gravity.START, 50f) //set drawer elevation (dimension)
        drawer_layout.setDrawerElevation(Gravity.END, 50f) //set drawer elevation (dimension)
        drawer_layout.setContrastThreshold(3f) //set maximum of contrast ratio between white text and background color.
        drawer_layout.setRadius(Gravity.START, 0f) //set end container's corner radius (dimension)

        drawer_layout.setRadius(Gravity.END, 0f)
        about_app = drawer_layout.findViewById(R.id.about_app)
        about_app.setOnClickListener { startActivity(Intent(this@MainActivity,AboutAppActivity::class.java)) }
        terms = drawer_layout.findViewById(R.id.terms)
        terms.setOnClickListener { startActivity(Intent(this,TermsActivity::class.java)) }
        call_us = drawer_layout.findViewById(R.id.call_us)
        call_us.setOnClickListener { startActivity(Intent(this,CallUsActivity::class.java))
        }
        settings = drawer_layout.findViewById(R.id.settings)
        settings.setOnClickListener { startActivity(Intent(this,SettingsActivity::class.java)) }
        chats = drawer_layout.findViewById(R.id.chats)
        chats.setOnClickListener { startActivity(Intent(this,ChatsActivity::class.java)) }
        profile = drawer_layout.findViewById(R.id.profile)
        profile.setOnClickListener { startActivity(Intent(this,ProfileActivity::class.java)) }
        notification = drawer_layout.findViewById(R.id.notifications)
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java)) }
        image = drawer_layout.findViewById(R.id.image)
        name = drawer_layout.findViewById(R.id.name)
        address = drawer_layout.findViewById(R.id.address)
        Glide.with(mContext).asBitmap().load(mSharedPrefManager.userData.avatar).into(image)
        name.text = mSharedPrefManager.userData.name!!
        address.text = mSharedPrefManager.userData.email
        menu.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }
//        orders = drawer_layout.findViewById(R.id.orders)
//        orders.setOnClickListener { startActivity(Intent(this,OrdersActivity::class.java)) }
        home = drawer_layout.findViewById(R.id.home)
        home.setOnClickListener { drawer_layout.closeDrawer(GravityCompat.START) }
        logout = drawer_layout.findViewById(R.id.logout)
        logout.setOnClickListener { logout() }
    }


    fun Count(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.CountOrders(mSharedPrefManager.userData.id!!)?.enqueue(object :Callback<MainResponse>{
            override fun onFailure(call: Call<MainResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
            }

            override fun onResponse(call: Call<MainResponse>, response: Response<MainResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        new_order.text = response.body()?.data?.current.toString()+"   طلب   "
                        current_order.text = response.body()?.data?.pending.toString()+"   طلب   "
                        canceled_order.text = response.body()?.data?.refuse.toString()+"   طلب   "
                        finished_order.text = response.body()?.data?.accepted.toString()+"   طلب   "
                        deleted_order.text = response.body()?.data?.deleted.toString()+"   طلب   "
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun logout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.logOut(mSharedPrefManager.userData.id!!,deviceID,"android",mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<AboutAppResponse> {
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        mSharedPrefManager.loginStatus=false
                        mSharedPrefManager.Logout()
                        CommonUtil.makeToast(applicationContext,response.body()?.data!!)
                        startActivity(Intent(this@MainActivity, SplashActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(applicationContext,response.body()?.msg!!)

                    }
                }
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Activate()
    }
    fun  Profile(){

        Client.getClient()?.create(Service::class.java)?.getProfile(mSharedPrefManager.userData.id!!,mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.body()?.value.equals("1")){
                    mSharedPrefManager.userData = response.body()?.data!!
                    Glide.with(mContext).asBitmap().load(mSharedPrefManager.userData.avatar).into(image)
                    Glide.with(mContext).asBitmap().load(mSharedPrefManager.userData.avatar).into(photo)
                    user_name.text = response.body()?.data?.name!!
                }else{
                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                }
            }

        })
    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
//                        val intent = Intent(this@MainActivity,ProfileService::class.java)
//                        intent.putExtra("result",result)
//                        intent.putExtra("mLat",mLat)
//                        intent.putExtra("mLang",mLang)
                       // startService(intent)
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                            startForegroundService(intent)
//                        }else {
//                            startService(intent)
//                        }
                        Client.getClient()?.create(Service::class.java)?.updateLat(mSharedPrefManager.userData.id!!,mLanguagePrefManager.appLanguage
                            ,result,mLat,mLang)?.enqueue(object :Callback<UserResponse>{
                            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                                CommonUtil.handleException(mContext,t)
                                t.printStackTrace()

                            }

                            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {

                                if (response.isSuccessful){
                                    if (response.body()?.value.equals("1")){

                                        mSharedPrefManager.userData = response.body()?.data!!

                                    }else{

                                    }
                                }
                            }

                        })





                    }


                } catch (e: IOException) {
                }



            }
        }
    }
    fun Activate(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Active(mSharedPrefManager.userData.id!!,0)?.enqueue(object:Callback<BaseResponse>{
            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        timer.cancel()
                        timer.purge()
                        finishAffinity()
                    }
                }
            }

        })
    }
}