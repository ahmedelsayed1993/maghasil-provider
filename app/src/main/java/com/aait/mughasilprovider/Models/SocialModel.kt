package com.aait.mughasilprovider.Models

import java.io.Serializable

class SocialModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var link:String?=null
}