package com.aait.mughasilprovider.Models

import java.io.Serializable

class OrderDetailsModel:Serializable {
    var username:String?=null
    var address:String?=null
    var lat:String?=null
    var lng:String?=null
    var user_id:Int?=null
    var status:String?=null
}