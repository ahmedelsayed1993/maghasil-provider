package com.aait.mughasilprovider.Models

import java.io.Serializable

class MainModel:Serializable {
    var current:Int?=null
    var pending:Int?=null
    var refuse:Int?=null
    var accepted:Int?=null
    var deleted:Int?=null
}