package com.aait.mughasilprovider.Models

import java.io.Serializable

class NotificationResponse:BaseResponse(),Serializable {
    var data:ArrayList<NotificationModel>?=null
}