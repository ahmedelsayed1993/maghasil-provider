package com.aait.mughasilprovider.Network

import com.aait.mughasilprovider.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Query

interface Service {

    @POST("sign-up-provider")
    fun SignUp(@Query("phone") phone:String,
               @Query("name") name:String,
               @Query("email") email:String?,
               @Query("address") address:String,
               @Query("lat") lat:String,
               @Query("lng") lng:String,
               @Query("another_number") another_number:String?,
               @Query("commercial") commercial:String,
               @Query("password") password:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String):Call<UserResponse>
    @Multipart
    @POST("sign-up-provider")
    fun SignUpwithImage(@Query("phone") phone:String,
               @Query("name") name:String,
               @Query("email") email:String?,
               @Query("address") address:String,
               @Query("lat") lat:String,
               @Query("lng") lng:String,
               @Query("another_number") another_number:String?,
               @Query("commercial") commercial:String,
               @Query("password") password:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Part avatar:MultipartBody.Part):Call<UserResponse>

    @POST("check-code")
    fun CheckCode(@Query("user_id") user_id:Int,
                  @Query("code") code:String,
                  @Query("lang") lang:String):Call<UserResponse>

    @POST("resend-code")
    fun ReSend(@Query("user_id") user_id:Int):Call<AboutAppResponse>

    @POST("sign-in")
    fun SignIn(@Query("phone") phone:String,
               @Query("password") password:String,
               @Query("device_id") device_id: String,
               @Query("device_type") device_type: String,
               @Query("lang") lang:String):Call<UserResponse>

    @POST("about")
    fun AboutApp():Call<AboutAppResponse>


    @POST("terms")
    fun Terms():Call<AboutAppResponse>
    @POST("forget-password")
    fun ForGot(@Query("phone") phone:String,
               @Query("lang") lang:String):Call<UserResponse>

    @POST("update-password")
    fun NewPass(@Query("user_id") user_id: Int,
                @Query("password") password:String,
                @Query("code") code:String,
                @Query("lang") lang: String):Call<UserResponse>
    @POST("contact-us")
    fun Contact(@Query("name") name:String?,
                @Query("email") email:String?,
                @Query("message") message:String?):Call<ContactUsResponse>

    @POST("log-out")
    fun logOut(@Query("user_id") user_id:Int,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Query("lang") lang:String):Call<AboutAppResponse>
    @POST("provider-notifications")
    fun Notifications(@Query("provider_id") user_id: Int):Call<NotificationResponse>

    @POST("count-orders")
    fun CountOrders(@Query("provider_id") provider_id:Int):Call<MainResponse>
    @POST("provider-orders")
    fun Orders(@Query("provider_id") provider_id:Int,
               @Query("status") status:String):Call<OrdersResponse>
    @POST("details-order")
    fun OrderDetails(@Query("provider_id") provider_id:Int,
                     @Query("order_id") order_id:Int,
                     @Query("action") action:String?):Call<OrderDetailsResponse>
    @POST("show-phone")
    fun switchPhone(@Query("user_id") user_id:Int,
                    @Query("switch") switch:Int?):Call<SwitchNotification>
    @POST("conversation")
    fun Conversation(@Query("lang") lang: String,
                     @Query("conversation_id") conversation_id:Int,
                     @Query("user_id") user_id: Int,
                     @Query("page") page:Int,
                     @Query("app_type") app_type:String):Call<ChatResponse>

    @POST("conversation-id")
    fun ConversationID(@Query("user_id") user_id:Int,
                       @Query("provider_id") provider_id:Int):Call<ConversationIdResponse>
    @POST("send-message")
    fun Send(@Query("lang") lang: String,
             @Query("user_id") user_id: Int,
             @Query("receiver_id") receiver_id:Int,
             @Query("conversation_id") conversation_id:Int,
             @Query("message") message:String):Call<SendChatResponse>
    @POST("my-conversations")
    fun Conversations(@Query("user_id") user_id:Int,
                      @Query("lang") lang:String):Call<ChatsResponse>

    @POST("edit-profile")
    fun getProfile(@Query("user_id") user_id:Int,
                   @Query("lang") lang: String):Call<UserResponse>

    @Multipart
    @POST("edit-profile")
    fun UpdateImage(@Query("user_id") user_id:Int,
                    @Query("lang") lang: String,
                    @Part avater:MultipartBody.Part):Call<UserResponse>

    @POST("edit-profile")
    fun update(@Query("user_id") user_id:Int,
               @Query("lang") lang: String,
               @Query("name") name:String,
               @Query("address") address:String,
               @Query("lat") lat:String,
               @Query("lng") lng: String,
               @Query("phone") phone:String):Call<UserResponse>
    @POST("edit-profile")
    fun updateLat(@Query("user_id") user_id:Int,
               @Query("lang") lang: String,
               @Query("address") address:String,
               @Query("lat") lat:String,
               @Query("lng") lng: String):Call<UserResponse>
    @POST("reset-password")
    fun resetPassword(@Query("lang") lang:String,
                      @Query("user_id") user_id: Int,
                      @Query("current_password") current_password:String,
                      @Query("password") password:String):Call<BaseResponse>
    @POST("provider-delete-order")
    fun deleteOrder(@Query("provider_id") provider_id:Int,
                    @Query("order_id") order_id:Int,
                    @Query("deleted") deleted:String):Call<AboutAppResponse>

    @POST("switch-notification")
    fun switch(@Query("user_id") user_id:Int,
               @Query("switch") switch:Int?):Call<SwitchNotification>

    @POST("delete-notification")
    fun delete(
               @Query("user_id") user_id:Int,
               @Query("notification_id") notification_id:Int):Call<AboutAppResponse>

    @POST("trsck-order-provider")
    fun track(@Query("provider_id") provider_id: Int,
              @Query("order_id") order_id: Int,
              @Query("confirm") confirm:Int?,
              @Query("status") status:String?):Call<AboutAppResponse>
    @POST("provider-active")
    fun Active(@Query("provider_id") provider_id: Int,
               @Query("active") active:Int):Call<BaseResponse>
}